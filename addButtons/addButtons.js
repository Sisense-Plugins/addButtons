//
// Add Navigation Links: Adds links to the navigation menu (top right corner)
//

prism.run([function(){

	//	Add handler for header menu buttons
	mod.directive('headerMenu',[function(){
		return {
			restrict: 'C',
			replace: false,
			link: function(scope,element,attrs){

				//	Loop through all the buttons to add
				prism.extraButtons.forEach(function(button){

					//	Create a new button object
					var newButton = {
						disable: false,
						id: button.id,
						title: button.label,
						url: button.link,
						visible: true
					}

					//	Add to the list
					scope.navItems.push(newButton);
				})
			}
		}
	}])
}])
